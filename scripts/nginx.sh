#!/usr/bin/env sh
yum install -y epel-release
yum install -y nginx
systemctl enable nginx
systemctl start nginx

systemctl enable firewalld
systemctl start firewalld
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

