#!/usr/bin/env sh
yum install -y httpd httpd-tools
systemctl enable httpd
systemctl start httpd

systemctl enable firewalld
systemctl start firewalld
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

