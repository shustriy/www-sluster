#!/usr/bin/env sh
yum install -y vim

# print fill hostname in console
#sed -i 's/\[\\u@\\h \\W\]/\[\\u@\\H \\W\]/g' /etc/bashrc
# White hostname in console
sed -i 's/\[\\u@\\h \\W\]/\[\\u@\\e\[1;37m\\H\\e\[0m \\W\]/g' /etc/bashrc

